<?php


require_once 'app/Mage.php';
umask(0);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
ini_set('display_errors', 1);
ini_set('max_execution_time', 600);

/* 记得把 csv 中的第一行，即 field名 删掉 */
$file = fopen("import_customers8.csv","r");

$data = array();

while( !feof($file) )
{
    $temp = array(
        'email'=>' ',
        'password_hash'=>' ',
        '_website'=>'base',
        'website_id'=>'1',
        '_store'=>'default',
        'store_id'=>'1',
        'created_in'=>'Default Store View',
        'group_id'=>'1',
        'firstname'=>' ',
        'lastname'=>' '
    );

    $oneline = fgetcsv($file);
    //var_dump($oneline);

    $temp['email'] = $oneline[0];
    $temp['password_hash'] = $oneline[1];

    $data[] = $temp;
}


$import = Mage::getModel('fastsimpleimport/import');
try {
    $import->processCustomerImport($data);
} catch (Exception $e) {
    print_r($import->getErrorMessages());
}
